class Meal < ActiveRecord::Base
  DAY_MOMENTS = { 0 => 'Mañana', 1 => 'Media Mañana', '2' => 'Almuerzo',
    '3' => 'Media Tarde', '4' => 'Tarde', '5' => 'Noche' }

  belongs_to :diet_plan

  def self.day_moments
    DAY_MOMENTS.map { |k,v| [v,k] }
  end
end
