class DietPlanAssignment < ActiveRecord::Base
  PRIORITIES = { 0 => 'Principal', 1 => 'Secundario' }

  belongs_to :deportist
  belongs_to :diet_plan

  validate :age_range

  before_save :update_previous_main_plan

  def self.priorities
    PRIORITIES.map { |k,v| [v,k] }
  end

  def age_range
    if diet_plan.from_age && diet_plan.to_age
      if deportist.age < diet_plan.from_age || deportist.age > diet_plan.to_age
        errors.add(:deportist, "La edad del deportista debe estar entre #{diet_plan.from_age} y #{diet_plan.to_age} años.")
      end
    end
  end

  def update_previous_main_plan
    if deportist.main_diet_plan
      previous_main_assignment = deportist.diet_plan_assignments.where(priority: 0).first
      previous_main_assignment.update_columns(priority: 1)
    end
  end

end
