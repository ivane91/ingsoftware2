class DietPlan < ActiveRecord::Base
  TYPES = { 0 => 'Generico', 1 => 'Especifico' }
  DURATION_TYPES = { 0 => 'Dias', 1 => 'Semanas', 2 => 'Meses' }
  FREQUENCIES = { 0 => 'Diaria', 1 => 'Semanal', 2 => 'Quincenal', 3 => 'Mensual' }

  belongs_to :category

  has_many :diet_plan_assignments, dependent: :destroy
  has_many :deportists, through: :diet_plan_assignments
  has_many :meals, dependent: :destroy

  validates :name, uniqueness: true, presence: true, format: { with: /\A[a-zA-Z ]+\z/ }
  validates :goal, presence: true, format: { with: /\A[a-zA-Z ]+\z/ }
  validates :duration, presence: true, numericality: true
  validates :meals, presence: true

  validates :from_age, numericality: true, allow_nil: true
  validates :to_age, numericality: true, allow_nil: true
  validates :from_imc, numericality: true, allow_nil: true
  validates :to_imc, numericality: true, allow_nil: true

  accepts_nested_attributes_for :meals, :reject_if => :all_blank, :allow_destroy => true

  scope :search, -> (text) { where('name like ? or goal like ?', "%#{text}%", "%#{text}%") }

  def auto_generate_id
    "PD-#{rand.to_s[2..7]}"
  end

  def self.available_plan_types
    TYPES.map { |k,v| [v,k] }
  end

  def self.duration_types
    DURATION_TYPES.map { |k,v| [v,k] }
  end

  def self.frequencies
    FREQUENCIES.map { |k,v| [v,k] }
  end

  def age_range
    if from_age && to_age
      (from_age..to_age)
    end
  end

  def check_deportists_plans
    deportists.each do |d|
      return false if (d.diet_plans.count - 1).zero?
    end

    true
  end
end
