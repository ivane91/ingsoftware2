class Deportist < ActiveRecord::Base
  has_many :diet_plan_assignments, dependent: :destroy
  has_many :diet_plans, through: :diet_plan_assignments

  def main_diet_plan
    diet_plans.joins(:diet_plan_assignments).
      where(diet_plan_assignments: { priority: 0 }).first
  end

  def secondary_diet_plans
    diet_plans.joins(:diet_plan_assignments).
    where(diet_plan_assignments: { priority: 1 })
  end
end
