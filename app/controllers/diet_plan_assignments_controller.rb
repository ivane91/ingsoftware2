class DietPlanAssignmentsController < ApplicationController
  def new
    @deportist = Deportist.find(params[:deportist_id])
    @diet_plan_assignment = DietPlanAssignment.new
    @diet_plans = DietPlan.all
  end

  def create
    @deportist = Deportist.find(params[:deportist_id])
    @diet_plan_assignment = DietPlanAssignment.new(plan_assignment_params)
    @diet_plan_assignment.deportist = @deportist
    @diet_plans = DietPlan.all

    @diet_plan_assignment.save

    render :new
  end

  private

  def plan_assignment_params
    params.require(:diet_plan_assignment).permit(:priority, :diet_plan_id)
  end
end
