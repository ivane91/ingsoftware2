class DietPlansController < ApplicationController
  def new
    @diet_plan = DietPlan.new
    @categories = Category.all
  end

  def confirm_deletion
    @diet_plan = DietPlan.find(params[:id])
  end

  def create
    @diet_plan = DietPlan.new(diet_plan_params)
    @categories = Category.all

    if @diet_plan.save
      redirect_to root_path
    else
      render :new
    end
  end

  def destroy
    @diet_plan = DietPlan.find(params[:id])

    if @diet_plan.check_deportists_plans
      @diet_plan.destroy

      redirect_to root_path
    else
      render :confirm_deletion
    end
  end

  private

  def diet_plan_params
    params.require(:diet_plan).permit(:name, :goal, :custom_id, :category_id, :plan_type,
      :from_age, :to_age, :duration, :duration_type, :frequency, :from_imc, :to_imc,
      meals_attributes: [:calories, :portion, :day, :day_moment, :_destroy])
  end
end
