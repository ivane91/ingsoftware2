class DeportistsController < ApplicationController
  def index
    @deportists = Deportist.all
  end

  def new
    @deportist = Deportist.new
  end

  def create
    @deportist = Deportist.new(deportist_params)

    if @deportist.save
      redirect_to deportists_path
    else
      render :new
    end
  end

  private

  def deportist_params
    params.require(:deportist).permit(:name, :age, :imc, :number)
  end
end
