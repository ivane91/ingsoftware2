class HomeController < ApplicationController
  def index
    @diet_plans = if params[:filter]
      DietPlan.search(params[:filter])
    else
      DietPlan.all
    end
  end
end
