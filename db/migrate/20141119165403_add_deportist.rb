class AddDeportist < ActiveRecord::Migration
  def change
    create_table :deportists do |t|
      t.string :name
      t.string :imc
      t.integer :priority
      t.integer :age

      t.timestamps
    end
  end
end
