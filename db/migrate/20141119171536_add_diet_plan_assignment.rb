class AddDietPlanAssignment < ActiveRecord::Migration
  def change
    create_table :diet_plan_assignments do |t|
      t.references :deportist
      t.references :diet_plan
      t.integer :priority
      t.timestamps
    end
  end
end
