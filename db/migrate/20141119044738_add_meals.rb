class AddMeals < ActiveRecord::Migration
  def change
    create_table :meals do |t|
      t.string :portion
      t.references :diet_plan
      t.integer :calories
      t.integer :day
      t.integer :day_moment
    end
  end
end
