class AddDietPlan < ActiveRecord::Migration
  def change
    create_table :diet_plans do |t|
      t.string :custom_id
      t.string :name
      t.text :goal
      t.integer :plan_type
      t.integer :from_age
      t.integer :to_age
      t.references :category
      t.integer :from_imc
      t.integer :to_imc
      t.integer :duration
      t.integer :duration_type      

      t.timestamps
    end
  end
end
