class ChangeDeportist < ActiveRecord::Migration
  def change
    remove_column :deportists, :priority
    add_column :deportists, :number, :integer
  end
end
