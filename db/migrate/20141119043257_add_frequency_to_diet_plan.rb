class AddFrequencyToDietPlan < ActiveRecord::Migration
  def change
    add_column :diet_plans, :frequency, :integer
  end
end
