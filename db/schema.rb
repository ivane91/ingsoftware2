# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141119171536) do

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "deportists", force: true do |t|
    t.string   "name"
    t.string   "imc"
    t.integer  "age"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "number"
  end

  create_table "diet_plan_assignments", force: true do |t|
    t.integer  "deportist_id"
    t.integer  "diet_plan_id"
    t.integer  "priority"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "diet_plans", force: true do |t|
    t.string   "custom_id"
    t.string   "name"
    t.text     "goal"
    t.integer  "plan_type"
    t.integer  "from_age"
    t.integer  "to_age"
    t.integer  "category_id"
    t.integer  "from_imc"
    t.integer  "to_imc"
    t.integer  "duration"
    t.integer  "duration_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "frequency"
  end

  create_table "meals", force: true do |t|
    t.string  "portion"
    t.integer "diet_plan_id"
    t.integer "calories"
    t.integer "day"
    t.integer "day_moment"
  end

end
